The SOP (Statement of Purpose) season is back! and I got many requests
from my friends to review and help them polish up their SOPs. Here is
my LaTeX template for SOP. Please feel free to use it.

Requirement
-----------

 + It uses `Adobe Caslon Pro` font family. If you don't have them, you
 cannot compile the SOP. For more information about the fonts, please
 visit
 [here](http://store1.adobe.com/cfusion/store/html/index.cfm?store=OLS-US&event=displayFontPackage&code=1712)

How to Compile
--------------

 It uses `xelatex` to use custom fonts. After modifying the downloaded
 `main.tex`, type the following on the terminal:

    $ xelatex main.tex

 I only tested it on OSX machines, you may have problems on other
 platforms such as Windows or Linux.
